#pragma once

#include <iostream>
#include "kod.cpp"

using namespace std;

char tab[3][3];

struct wspolrzendne {
	int x;
	int y;
};

char koniec();
void sprawdz();
bool kon();
char sprawdz_pion();
char sprawdz_poziom();
void wypelnij();
char sprawdz_przekatne();
void aktualgracza(char g);
bool polewolne(int pole);
void wyznaczgracza(char *gracz);
wspolrzendne zamien_liczcbe_na_wsp(int z);
void wstaw_znak(int pole, char gracz);
void wyswietl();
